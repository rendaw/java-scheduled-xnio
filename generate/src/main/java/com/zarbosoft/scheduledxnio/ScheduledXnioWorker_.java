package com.zarbosoft.scheduledxnio;

import org.xnio.XnioExecutor;
import org.xnio.XnioWorker;

import java.util.concurrent.*;

public abstract class ScheduledXnioWorker_ implements ScheduledExecutorService {
	public final XnioWorker inner;

	protected ScheduledXnioWorker_(final XnioWorker inner) {
		this.inner = inner;
	}

	@Override
	public ScheduledFuture<?> schedule(final Runnable command, final long delay, final TimeUnit unit) {
		return new Key(inner.getIoThread().executeAfter(command, delay, unit));
	}

	@Override
	public <V> ScheduledFuture<V> schedule(final Callable<V> callable, final long delay, final TimeUnit unit) {
		throw new AssertionError("Not implemented");
	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(
			final Runnable command, final long initialDelay, final long period, final TimeUnit unit
	) {
		return new Key(inner.getIoThread().executeAtInterval(command, period, unit));
	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(
			final Runnable command, final long initialDelay, final long delay, final TimeUnit unit
	) {
		throw new AssertionError("Not implemented");
	}

	private static class Key implements ScheduledFuture<Object> {
		private final XnioExecutor.Key key;

		public Key(final XnioExecutor.Key key) {
			this.key = key;
		}

		@Override
		public long getDelay(final TimeUnit unit1) {
			throw new AssertionError("Not implemented");
		}

		@Override
		public int compareTo(final Delayed o) {
			throw new AssertionError("Not implemented");
		}

		@Override
		public boolean cancel(final boolean mayInterruptIfRunning) {
			return key.remove();
		}

		@Override
		public boolean isCancelled() {
			throw new AssertionError("Not implemented");
		}

		@Override
		public boolean isDone() {
			throw new AssertionError("Not implemented");
		}

		@Override
		public Object get() throws InterruptedException, ExecutionException {
			throw new AssertionError("Not implemented");
		}

		@Override
		public Object get(
				final long timeout, final TimeUnit unit
		) throws InterruptedException, ExecutionException, TimeoutException {
			throw new AssertionError("Not implemented");
		}
	}
}
