package com.zarbosoft.internal.scheduledxnio;

import com.squareup.javapoet.*;
import com.zarbosoft.scheduledxnio.ScheduledXnioWorker_;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.xnio.OptionMap;
import org.xnio.Xnio;
import org.xnio.XnioWorker;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static javax.lang.model.element.Modifier.*;

public class GenerateTask extends Task {

	public Path path;

	public void setPath(final String path) {
		this.path = Paths.get(path);
	}

	public static void clone(
			final TypeSpec.Builder out, final Method source, final String statement, final Object... statementArgs
	) {
		final List<ParameterSpec> parameters = new ArrayList<>();
		for (final Class<?> parameter : source.getParameterTypes()) {
			parameters.add(ParameterSpec.builder(parameter, String.format("arg%s", parameters.size())).build());
		}
		final List<javax.lang.model.element.Modifier> modifiers = new ArrayList<>();
		if (Modifier.isStatic(source.getModifiers()))
			modifiers.add(STATIC);
		if (Modifier.isPrivate(source.getModifiers()))
			modifiers.add(PRIVATE);
		if (Modifier.isPublic(source.getModifiers()))
			modifiers.add(PUBLIC);
		if (Modifier.isProtected(source.getModifiers()))
			modifiers.add(PROTECTED);
		final MethodSpec.Builder methodOut = MethodSpec
				.methodBuilder(source.getName())
				.addModifiers(modifiers)
				.addParameters(parameters)
				.returns(source.getReturnType());
		for (final Class<?> raises : source.getExceptionTypes())
			methodOut.addException(raises);
		methodOut.addStatement(
				statement.replace("$P",
						parameters.stream().map(parameter -> parameter.name).collect(Collectors.joining(", "))
				),
				statementArgs
		);
		out.addMethod(methodOut.build());
	}

	@Override
	public void execute() throws BuildException {
		final ClassName name = ClassName.get("com.zarbosoft.scheduledxnio", "ScheduledXnioWorker");
		final TypeSpec.Builder outType = TypeSpec
				.classBuilder(name)
				.superclass(ScheduledXnioWorker_.class)
				.addSuperinterface(ScheduledExecutorService.class)
				.addModifiers(PUBLIC);

		outType.addMethod(MethodSpec
				.constructorBuilder()
				.addModifiers(PRIVATE)
				.addParameter(XnioWorker.class, "inner")
				.addStatement("super(inner)")
				.build());

		outType.addMethod(MethodSpec
				.methodBuilder("createWorker")
				.addModifiers(PUBLIC, STATIC)
				.returns(name)
				.addParameter(OptionMap.class, "options")
				.addException(IOException.class)
				.addStatement("return new $T($T.getInstance().createWorker(options))", name, ClassName.get(Xnio.class))
				.build());
		for (final Method method : XnioWorker.class.getMethods()) {
			System.out.format("method %s\n", method.getName());
			if (!Modifier.isPublic(method.getModifiers()) ||
					Modifier.isStatic(method.getModifiers()) ||
					Modifier.isFinal(method.getModifiers())) {
				// nop
			} else {
				clone(outType, method, String.format("%sinner.%s($P)",
						method.getReturnType() == void.class ? "" : "return ",
						method.getName()
				));
			}
		}

		uncheck(() -> Files.createDirectories(path));
		uncheck(() -> JavaFile.builder(name.packageName(), outType.build()).build().writeTo(path));
	}
}
